﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc
{
    public class MeasureModel
    {
        public long DateTime { get; set; }
        public double Value { get; set; }
    }
}

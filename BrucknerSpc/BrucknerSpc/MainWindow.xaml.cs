﻿using BrucknerSpc.DataAccess;
using BrucknerSpc.MVC.Controller;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrucknerSpc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, MainWindowControl
    {
        private MainController controller;
        public MainWindow()
        {
            this.controller = new MainController();
            InitializeComponent();

            
        }
        #region MainWindowControl interface implementation
        public void AddDataValue(FullSetDisplayValue value)
        {
            //GraphData test = new GraphData()
            //{
            //    ActualValue = value.ValueTempOne.ActualValue,
            //    SpecifiedValue = value.ValueTempOne.SetValue,
            //    MaxValue = value.ValueTempOne.MaxValue,
            //    MinValue = value.ValueTempOne.MinValue,
            //    Time = value.ValueTempOne.DateTime.ToString(Properties.Settings.Default.XValuesDateTimeFormat)
            //};
            this.chartTmpOne.AddValue(value.ValueTempOne);
            this.chartTmpTwo.AddValue(value.ValueTempTwo);
            this.chartTmpThree.AddValue(value.ValueTempThree);
            this.chartTmpFour.AddValue(value.ValueTempFour);
            this.chartTmpFive.AddValue(value.ValueTempFive);
            this.chartTmpSix.AddValue(value.ValueTempSix);
        }
        #endregion
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.controller.ViewLoaded(this);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.controller.StopController();
        }


        
    }
}

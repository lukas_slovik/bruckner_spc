﻿using BrucknerSpc.DataAccess;
using BrucknerSpc.MVC.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc.MVC.Controller
{
    public class MainController : ControllerTasksCore
    {
        private MainWindowControl view;
        public MainController()
        {
            this.view = null;
        }

        public void ViewLoaded(MainWindowControl view)
        {
            this.view = view;
            AddTask(new UpdateGraphData(this));
        }

        public void AddValue(FullSetDisplayValue value)
        {
            this.view.AddDataValue(value);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrucknerSpc.MVC.Controller;
using System.Threading;
using BrucknerSpc.DataAccess;

namespace BrucknerSpc.MVC.Task
{
    public class UpdateGraphData : Runnable
    {
        private MainController controller;
        private bool isReading;
        public UpdateGraphData(MainController mvcController) : base(mvcController)
        {
            this.controller = mvcController;
            this.isReading = true;
        }

        public override void StopTask()
        {
            this.isReading = false;
        }

        protected override void RunResult()
        {

        }

        protected override void RunTask()
        {
            DateTime newValue = DateTime.Now.AddSeconds(-Properties.Settings.Default.BackInTimeInSeconds);
            FullSetDisplayValue dispVal;
            while (this.isReading)
            {
                List<MeasuredParameter> allParameters = MeasuredParameterDao.Instance.AllParameters;
                List<SpcData> newValues = SpcDataDao.Instance.NewerData(newValue);
                foreach (SpcData machineVal in newValues)
                {
                    dispVal = new FullSetDisplayValue();
                    foreach (MeasuredParameter parameter in allParameters)
                    {
                        GraphData zoneValue = new GraphData();
                        zoneValue.Time = machineVal.TimeSpan.ToString(Properties.Settings.Default.XValuesDateTimeFormat);
                        zoneValue.ActualValue = machineVal.RealValue;
                        zoneValue.Cl = machineVal.Average;
                        zoneValue.Lcl = machineVal.Average - Properties.Settings.Default.SpcE2Parameter * machineVal.AverageMR;
                        zoneValue.Ucl = machineVal.Average + Properties.Settings.Default.SpcE2Parameter * machineVal.AverageMR;
                        dispVal.SetValue(parameter.IdMeasuredParameter, zoneValue);
                    }
                    this.controller.AddValue(dispVal);
                    newValue = machineVal.TimeSpan;
                }
                Thread.Sleep(Properties.Settings.Default.DataRefreshTime);
            }
            TaskCompleted();
        }
    }
}

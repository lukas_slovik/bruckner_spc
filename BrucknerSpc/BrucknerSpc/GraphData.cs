﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc
{
    public class GraphData
    {
        public string Time { get; set; }
        public double ActualValue { get; set; }
        public double Cl { get; set; }
        public double Ucl { get; set; }
        public double Lcl { get; set; }


    }
}

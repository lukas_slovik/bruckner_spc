﻿using BrucknerSpc.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc
{
    public interface MainWindowControl
    {
        void AddDataValue(FullSetDisplayValue value);
    }
}

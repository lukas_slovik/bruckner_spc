﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Configurations;
using System.Threading;
using BrucknerSpc.DataAccess;

namespace BrucknerSpc
{
    public partial class ConstantChangesChart : UserControl, INotifyPropertyChanged
    {
        private double _axisMax;
        private double _axisMin;
        private long yearStartTicks;
        public ConstantChangesChart()
        {
            InitializeComponent();

            var mapper = Mappers.Xy<MeasureModel>()
                .X(model => model.DateTime)   //use DateTime.Ticks as X
                .Y(model => model.Value);           //use the value property as Y
            Charting.For<MeasureModel>(mapper);
            ActualValues = new ChartValues<MeasureModel>();
            SetValues = new ChartValues<MeasureModel>();
            MaxValues = new ChartValues<MeasureModel>();
            MinValues = new ChartValues<MeasureModel>();
            this.yearStartTicks = (new DateTime(DateTime.Now.Year, 1, 1)).Ticks;
            DateTimeFormatter = value => new DateTime(yearStartTicks + (long)value).ToString(Properties.Settings.Default.XValuesDateTimeFormat);
            AxisStep = TimeSpan.FromSeconds(Properties.Settings.Default.XValueStepInSeconds).Ticks;
            AxisUnit = TimeSpan.TicksPerSecond * Properties.Settings.Default.XValueUnitInSeconds;
            SetAxisLimits(DateTime.Now);
            DataContext = this;
        }

        public ChartValues<MeasureModel> ActualValues { get; set; }
        public ChartValues<MeasureModel> SetValues { get; set; }
        public ChartValues<MeasureModel> MaxValues { get; set; }
        public ChartValues<MeasureModel> MinValues { get; set; }
        public Func<double, string> DateTimeFormatter { get; set; }
        public double AxisStep { get; set; }
        public double AxisUnit { get; set; }

        public double AxisMax
        {
            get { return _axisMax; }
            set
            {
                _axisMax = value;
                OnPropertyChanged("AxisMax");
            }
        }
        public double AxisMin
        {
            get { return _axisMin; }
            set
            {
                _axisMin = value;
                OnPropertyChanged("AxisMin");
            }
        }


        //public void AddValue(DisplayValue data)
        //{
        //    ActualValues.Add(new MeasureModel
        //    {
        //        DateTime = (data.DateTime.Ticks - this.yearStartTicks),
        //        Value = data.ActualValue,
        //    });
        //    SetValues.Add(new MeasureModel
        //    {
        //        DateTime = (data.DateTime.Ticks - this.yearStartTicks),
        //        Value = data.SetValue,
        //    });
        //    MaxValues.Add(new MeasureModel
        //    {
        //        DateTime = (data.DateTime.Ticks - this.yearStartTicks),
        //        Value = data.MaxValue,
        //    });
        //    MinValues.Add(new MeasureModel
        //    {
        //        DateTime = (data.DateTime.Ticks - this.yearStartTicks),
        //        Value = data.MinValue,
        //    });
        //    if (ActualValues.Count > Properties.Settings.Default.MaxValuesDisplayed) ActualValues.RemoveAt(0);
        //    if (SetValues.Count > Properties.Settings.Default.MaxValuesDisplayed) SetValues.RemoveAt(0);
        //    if (MaxValues.Count > Properties.Settings.Default.MaxValuesDisplayed) MaxValues.RemoveAt(0);
        //    if (MinValues.Count > Properties.Settings.Default.MaxValuesDisplayed) MinValues.RemoveAt(0);
        //    SetAxisLimits(data.DateTime);
        //}

        private void SetAxisLimits(DateTime now)
        {
            AxisMax = (now.Ticks - this.yearStartTicks) + TimeSpan.FromSeconds(1).Ticks;
            AxisMin = (now.Ticks - this.yearStartTicks) - TimeSpan.FromSeconds(Properties.Settings.Default.XValueTotalDisplayedSeconds).Ticks;
        }

        private void InjectStopOnClick(object sender, RoutedEventArgs e)
        {
        }

        public string GraphTitle
        {
            get { return this.graphTitleLb.Content.ToString(); }
            set
            {
                this.graphTitleLb.Content = value;
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}

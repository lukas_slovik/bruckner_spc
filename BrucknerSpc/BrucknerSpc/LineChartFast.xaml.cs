﻿using BrucknerSpc.Log;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrucknerSpc
{
    /// <summary>
    /// Interaction logic for LineChartFast.xaml
    /// </summary>
    public partial class LineChartFast : UserControl
    {
        public LineChartFast()
        {
            InitializeComponent();
            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Aktuálna hodnota",
                    Values = new ChartValues<double> (),
                    LineSmoothness = 0.2,
                    PointGeometry=null,
                    StrokeThickness=1,
                    Stroke=new SolidColorBrush(Color.FromArgb(255,243,67,54)),
                    Fill=new SolidColorBrush(Color.FromArgb(0, 0, 0, 255)),
                    Foreground=new SolidColorBrush(Color.FromArgb(255,10,149,54))
                },
                new LineSeries
                {
                    Title = "Centrála priamka",
                    Values = new ChartValues<double> (),
                    LineSmoothness = 0.2,
                    PointGeometry=null,
                    StrokeThickness=1,
                    Stroke=new SolidColorBrush(Color.FromArgb(255,10,149,54)),
                    Fill=new SolidColorBrush(Color.FromArgb(0, 0, 0, 255)),
                    Foreground=new SolidColorBrush(Color.FromArgb(255,10,149,54))
                },
                new LineSeries
                {
                    Title = "Horná regulačná medza",
                    Values =  new ChartValues<double> (),
                    LineSmoothness = 0.2,
                    PointGeometry=null,
                    StrokeThickness=1,
                    Stroke=new SolidColorBrush(Color.FromArgb(255,96,17,191)),
                    Fill=new SolidColorBrush(Color.FromArgb(0, 0, 0, 255)),
                    Foreground=new SolidColorBrush(Color.FromArgb(255,10,149,54))
                },
                new LineSeries
                {
                    Title = "Dolná regulačná medza",
                    Values =  new ChartValues<double> (),
                    LineSmoothness = 0.2,
                    PointGeometry=null,
                    StrokeThickness=1,
                    Stroke=new SolidColorBrush(Color.FromArgb(255,211,183,10)),
                    Fill=new SolidColorBrush(Color.FromArgb(0, 0, 0, 255)),
                    Foreground=new SolidColorBrush(Color.FromArgb(255,10,149,54))
                }
            };

            Labels = new List<string>();
            double valueY = 0;
            for (int time = 0; time < Properties.Settings.Default.XValueTotalDisplayedSeconds; time = time + Properties.Settings.Default.XValueStepInSeconds)
            {
                Labels.Add(time.ToString());
                SeriesCollection[0].Values.Add(valueY);
                SeriesCollection[1].Values.Add(valueY);
                SeriesCollection[2].Values.Add(valueY);
                SeriesCollection[3].Values.Add(valueY);
                valueY++;
            }
            YFormatter = value => value.ToString();
            DataContext = this;
        }
        public void AddValue(GraphData value)
        {
            try
            {
                Labels.Add(value.Time);
                Labels.RemoveAt(0);
                SeriesCollection[0].Values.Add(value.ActualValue);
                SeriesCollection[0].Values.RemoveAt(0);
                SeriesCollection[1].Values.Add(value.Cl);
                SeriesCollection[1].Values.RemoveAt(0);
                SeriesCollection[2].Values.Add(value.Ucl);
                SeriesCollection[2].Values.RemoveAt(0);
                SeriesCollection[3].Values.Add(value.Lcl);
                SeriesCollection[3].Values.RemoveAt(0);
            }
            catch (Exception e)
            {
                EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
            }
        }

        public SeriesCollection SeriesCollection { get; set; }
        public List<string> Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }

    }
}

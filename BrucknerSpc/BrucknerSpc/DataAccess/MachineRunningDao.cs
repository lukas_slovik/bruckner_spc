﻿using BrucknerSpc.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc.DataAccess
{
    public class MachineRunningDao
    {
        private static MachineRunningDao instance;

        public static MachineRunningDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MachineRunningDao();
                return instance;
            }
        }

        private MachineRunningDao()
        {

        }

        public bool IsRunning()
        {
            using (var Context = new MESERPEntities())
            {
                var requestQuery = from piece in Context.FinishingPieces
                                   where piece.Running == true
                                   select piece;

                try
                {
                    FinishingPiece foundPiece = requestQuery.FirstOrDefault<FinishingPiece>();
                    return foundPiece != null;
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return false;
        }

        public bool WasRunning(DateTime valueTime)
        {
            using (var Context = new MESERPEntities())
            {
                var requestQuery = from piece in Context.FinishingPieces
                                   where ((valueTime>=piece.StartTime && valueTime<piece.StopTime) || (valueTime >= piece.StartTime && piece .StopTime== null) ) && piece.Canceled==false
                                   select piece;

                try
                {
                    FinishingPiece foundPiece = requestQuery.FirstOrDefault<FinishingPiece>();
                    return foundPiece != null;
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return false;
        }
    }
}

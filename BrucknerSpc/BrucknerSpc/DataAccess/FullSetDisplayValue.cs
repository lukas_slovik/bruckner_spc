﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc.DataAccess
{
    public class FullSetDisplayValue
    {
        private GraphData[] values;
        public GraphData ValueTempOne
        {
            get
            {
                return this.values[0];
            }
            set
            {
                this.values[0] = value;
            }
        }
        public GraphData ValueTempTwo
        {
            get
            {
                return this.values[1];
            }
            set
            {
                this.values[1] = value;
            }
        }
        public GraphData ValueTempThree
        {
            get
            {
                return this.values[2];
            }
            set
            {
                this.values[2] = value;
            }
        }
        public GraphData ValueTempFour
        {
            get
            {
                return this.values[3];
            }
            set
            {
                this.values[3] = value;
            }
        }
        public GraphData ValueTempFive
        {
            get
            {
                return this.values[4];
            }
            set
            {
                this.values[4] = value;
            }
        }
        public GraphData ValueTempSix
        {
            get
            {
                return this.values[5];
            }
            set
            {
                this.values[5] = value;
            }
        }
        public GraphData SetValue(int idMeasuredParameter, GraphData data)
        {
            if (this.values.Length > idMeasuredParameter - 1)
            {
                this.values[idMeasuredParameter - 1] = data;
                return this.values[idMeasuredParameter - 1];
            }
            return null;
        }
        public FullSetDisplayValue()
        {
            values = new GraphData[6];
        }
    }
}

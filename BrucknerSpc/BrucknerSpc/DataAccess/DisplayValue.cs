﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc.DataAccess
{
    public class DisplayValue
    {
        public DateTime DateTime { get; set; }
        public double ActualValue { get; set; }
        public double SetValue { get; set; }
        public double MaxValue { get; set; }
        public double MinValue { get; set; }
    }
}

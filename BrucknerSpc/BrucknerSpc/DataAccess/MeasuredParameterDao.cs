﻿using BrucknerSpc.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc.DataAccess
{
    public class MeasuredParameterDao : TimeRelevant
    {
        private static MeasuredParameterDao instance;

        public static MeasuredParameterDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new MeasuredParameterDao();
                return instance;
            }
        }
        private List<MeasuredParameter> allParameters;
        private object allParametersLock;
        private MeasuredParameterDao()
        {
            this.allParametersLock = new object();
            this.allParameters = null;
        }
        protected override bool NotInitialized
        {
            get
            {
                return this.allParameters == null;
            }
        }

        protected override int TimeOutPeriod
        {
            get
            {
                return Properties.Settings.Default.MeasuredParameterRefreshTime;
            }
        }

        public List<MeasuredParameter> AllParameters
        {
            get
            {
                if (ShouldRefresh)
                {
                    using (var Context = new BrucknerEntities())
                    {
                        var requestQuery = from item in Context.MeasuredParameters
                                           select item;
                        try
                        {
                            lock (this.allParametersLock)
                            {
                                this.allParameters = requestQuery.ToList<MeasuredParameter>();
                            }
                            Updated(DateTime.Now);
                        }
                        catch (Exception e)
                        {
                            EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                        }
                    }
                }
                lock (this.allParametersLock)
                {
                    if (this.allParameters != null)
                        return new List<MeasuredParameter>(this.allParameters);
                }
                return new List<MeasuredParameter>();
            }
        }
    }
}

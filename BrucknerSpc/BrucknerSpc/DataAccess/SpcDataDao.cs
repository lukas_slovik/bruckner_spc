﻿using BrucknerSpc.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrucknerSpc.DataAccess
{
    public class SpcDataDao
    {
        private static SpcDataDao instance;

        public static SpcDataDao Instance
        {
            get
            {
                if (instance == null)
                    instance = new SpcDataDao();
                return instance;
            }
        }

        private SpcDataDao()
        {

        }

        public List<SpcData> NewerData(DateTime lastDate)
        {
            using (var Context = new BrucknerEntities())
            {
                var requestQuery = from machVal in Context.SpcDatas
                                   where machVal.TimeSpan > lastDate
                                   orderby machVal.TimeSpan
                                   select machVal;
                try
                {
                    return requestQuery.ToList<SpcData>();
                }
                catch (Exception e)
                {
                    EventLogMultiThread.AddErrorLog(Properties.Settings.Default.LogSourceName, System.Reflection.MethodBase.GetCurrentMethod(), e);
                }
            }
            return new List<SpcData>();
        }
    }
}
